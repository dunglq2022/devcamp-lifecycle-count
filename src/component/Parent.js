import { Component } from "react";
import Count from "./Count";

class Parent extends Component {
    constructor(props){
        super(props);

        this.state = {
            childDisplay: true
        }
    }

    updateDisplay = () => {
        this.setState({
            childDisplay: false
        })
    }
    render() {
        return(
            <>
            {this.state.childDisplay ? <Count display ={this.updateDisplay}/> : null}
            </>
        )
    }
}

export default Parent;