import { Component } from "react";

class Count extends Component{
    constructor(props) {
        super(props);

        this.state = {
            count: 1
        }
    }

    addCounter = () => {
        this.setState({
            count: this.state.count + 1
        })
    }

    componentDidMount() {
        console.log('Did mount thuc hien sau render')
        //Set gia tri
        setInterval(() => this.addCounter(), 1000)// truyen funtion callback
    }

    componentDidUpdate() {
        console.log('Did update thuc hien sau render')

        if(this.state.count === 10) {
            this.props.display();
        }
    }

    render(){
        console.log('Render: count', this.state.count)
        return(
            <>
            <p>Count: {this.state.count}</p>
            </>
        )
    }
}

export default Count;